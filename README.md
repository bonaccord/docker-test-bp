# Docker Test Instructions

## Summary

This project creates a Vagrant virtual machine environment in which it runs a containerised service using Docker.

The service runs a webpage (using NGINX) which is visible to both the Vagrant VM and the host OS hosting the Vagrant VM.

## Setup

Run `./setup.sh` in order to create and deploy all files with standardised parameters in a systematic way.

This will perform the following:

- Create a Vagrant file defining the Vagrant VM environment
- Define the VM networking and port forwarding
- Provision the VM using Ansible to install all requirements for Docker etc.

and:

- Create a Dockerfile defining the Docker environment
- Create the files to be passed into and used by the Docker service
- Start the Docker microservice

## Execution

The Vagrant VM and Docker service can be built (provisioned) and started as follows:

`vagrant reload --provision`

## Dependencies

An Ansible playbook and two Ansible include files are used to perform the Docker setup and Docker build & run respectively.

## Enhancement Ideas

- Add more debug information for provisioning 
- Use Ansible Galaxy Roles to paramterise the Docker build and run
- Extend testing framework beyond simple cURL command
- Use Jinja2 template files and filters with Ansible to deploy nginx.conf & index.html during provisioning
- Parameterise variable specification in `setup.sh` by using getopts and command-line switches 
