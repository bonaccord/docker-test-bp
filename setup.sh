#!/bin/bash

# Ensure vagrant installed

if [[ -z $(which vagrant) ]]; then
   echo "Error: Please install vagrant"
fi

# Setup variables

BASE_BOX="centos/7"
VAGRANT_CONFIG=./Vagrantfile
PROVISIONER=ansible
PUBLIC_IP="10.0.2.22"
GUEST_PORT_1=7000
HOST_PORT_1=9090
VM_NAME="bp-vagrant-vm"
SETUP_PLAYBOOK=site-vagrant-vm-setup.yml
DOCKERFILE=Dockerfile

################################
# Create a Vagrantfile 
################################

cat > $VAGRANT_CONFIG <<EOF
Vagrant.configure(2) do |config|
  config.vm.box = "${BASE_BOX}"
  config.vm.define "${VM_NAME}" do |t|
  end
  #config.vm.network "private_network", ip: "192.168.3.222"
  #config.vm.provider "virtualbox" do |vb|
  #  vb.gui = true
  #end
  config.vm.network "forwarded_port", guest: $GUEST_PORT_1, host: $HOST_PORT_1 
  config.vm.provision "ansible_local" do |ansible|
    ansible.playbook = "${SETUP_PLAYBOOK}"
    ansible.install_mode = "pip"
  end
end
EOF

################################
# Create a Dockerfile
################################
[[ ! -d docker-files ]] && mkdir docker-files
cat > docker-files/$DOCKERFILE <<EOF
FROM alpine:latest

RUN echo "http://dl-2.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN echo "http://dl-3.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN echo "http://dl-4.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN echo "http://dl-5.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories

RUN adduser -D -u 1000 -g 'www' www

RUN apk --update add nginx 
RUN mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.BACKUP
ADD nginx.conf /etc/nginx/nginx.conf

RUN mkdir -p /www
RUN chown -R www:www /var/lib/nginx
RUN chown -R www:www /www
RUN chmod 755 /www
ADD index.html /www/index.html
RUN chmod 644 /www/index.html

EXPOSE 80

RUN apk add openrc
#RUN rc-service nginx start

CMD ["nginx", "-g", "daemon off;"]
EOF

cat > docker-files/nginx.conf <<EOF
user                            www;
worker_processes                1;

error_log                       /var/log/nginx/error.log warn;
pid                             /var/run/nginx.pid;

events {
    worker_connections          1024;
}

http {
    include                     /etc/nginx/mime.types;
    default_type                application/octet-stream;
    sendfile                    on;
    access_log                  /var/log/nginx/access.log;
    keepalive_timeout           3000;
    server {
        listen                  80;
        root                    /www;
        index                   index.html index.htm;
        server_name             localhost;
        client_max_body_size    32m;
        error_page              500 502 503 504  /50x.html;
        location = /50x.html {
              root              /var/lib/nginx/html;
        }
    }
}
EOF

cat > docker-files/index.html <<EOF
<html>
<header><title>Test Page for NGINX Microservice</title></header>
<body>
Hello World!
</body>
</html>
EOF
